//
//  ViewController.swift
//  GCDBarrier
//
//  Created by 金鑫 on 2020/6/9.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let queue = DispatchQueue(label: "com.test.home", attributes: .concurrent)
        
        queue.async(flags: .barrier) {
            print("先要完成的任务: \(Thread.current)")
        }
        
        print("主线程任务1: ")
        
        queue.async {
            Thread.sleep(forTimeInterval: 2)
            print("完成任务 1: \(Thread.current)")
        }
        
        queue.async {
            Thread.sleep(forTimeInterval: 2)
            print("完成任务 2: \(Thread.current)")
        }
        
        queue.async {
            Thread.sleep(forTimeInterval: 2)
            print("完成任务 3: \(Thread.current)")
        }
        
        queue.async(flags: .barrier) {
            print("完所其他任务: \(Thread.current)")
        }
        print("主线程任务2: ")
    }


}

